Categories:Internet
License:GPLv2+
Web Site:https://www.cipherdyne.org/
Source Code:https://github.com/oneru/fwknop2
Issue Tracker:https://github.com/oneru/fwknop2/issues

Auto Name:Fwknop2
Summary:Client to send SPA packets to a fwknopd server
Description:
Allows you to send packets to an fwknopd service to remotely open ports on a
Linux machine or a router running OpenWrt. Fwknop2 has the ability to launch
an ssh client to start the login process through the newly opened port. It
also includes the ability to import keys via QR codes.
.

Repo Type:git
Repo:https://github.com/oneru/fwknop2.git

Build:1.0,3
    disable=pre-release
    commit=83416b654afd17c1e35d3f4b82fd109d8a8cf32c
    subdir=app
    gradle=yes
    srclibs=juicessh-plugin-library@cf1a9c3494f15c324fbb1e373b49b1b3a79b1926
    prebuild=sed -i -e '/raw.github.com\/Sonelli\/maven/d' build.gradle && \
        sed -i -e '/jcenter/amavenLocal()' ../build.gradle && \
        pushd $$juicessh-plugin-library$$ && \
        sed -i -e '/Dropbox/d' -e '/groupId/irepository(url: mavenLocal().url)' build.gradle && \
        gradle uploadArchives && \
        popd
    buildjni=no

Build:1.0,4
    commit=v1.0-release
    subdir=app
    gradle=yes
    srclibs=juicessh-plugin-library@cf1a9c3494f15c324fbb1e373b49b1b3a79b1926
    prebuild=sed -i -e '/raw.github.com\/Sonelli\/maven/d' -e '/juicessh/d' build.gradle && \
        sed -i -e '/jcenter/amavenLocal()' ../build.gradle && \
        mkdir -p ../libs/ && \
        pushd $$juicessh-plugin-library$$ && \
        sed -i -e '/Dropbox/d' -e '/groupId/irepository(url: mavenLocal().url)' build.gradle && \
        popd && \
        cp -fR $$juicessh-plugin-library$$ ../libs/ && \
        echo -e "\ninclude ':libs:juicessh-plugin-library'" >> ../settings.gradle && \
        sed -i -e '/support-v4/acompile project(":libs:juicessh-plugin-library")' build.gradle
    buildjni=no

Build:1.0.1,5
    commit=v1.0.1-release
    subdir=app
    gradle=yes
    srclibs=juicessh-plugin-library@cf1a9c3494f15c324fbb1e373b49b1b3a79b1926
    prebuild=sed -i -e '/raw.github.com\/Sonelli\/maven/d' -e '/juicessh/d' build.gradle && \
        sed -i -e '/jcenter/amavenLocal()' ../build.gradle && \
        mkdir -p ../libs/ && \
        pushd $$juicessh-plugin-library$$ && \
        sed -i -e '/Dropbox/d' -e '/groupId/irepository(url: mavenLocal().url)' build.gradle && \
        popd && \
        cp -fR $$juicessh-plugin-library$$ ../libs/ && \
        echo -e "\ninclude ':libs:juicessh-plugin-library'" >> ../settings.gradle && \
        sed -i -e '/support-v4/acompile project(":libs:juicessh-plugin-library")' build.gradle
    buildjni=no

Build:1.0.2,6
    commit=v1.0.2-release
    subdir=app
    gradle=yes
    srclibs=juicessh-plugin-library@cf1a9c3494f15c324fbb1e373b49b1b3a79b1926
    prebuild=sed -i -e '/raw.github.com\/Sonelli\/maven/d' -e '/juicessh/d' build.gradle && \
        sed -i -e '/jcenter/amavenLocal()' ../build.gradle && \
        mkdir -p ../libs/ && \
        pushd $$juicessh-plugin-library$$ && \
        sed -i -e '/Dropbox/d' -e '/groupId/irepository(url: mavenLocal().url)' build.gradle && \
        popd && \
        cp -fR $$juicessh-plugin-library$$ ../libs/ && \
        echo -e "\ninclude ':libs:juicessh-plugin-library'" >> ../settings.gradle && \
        sed -i -e '/support-v4/acompile project(":libs:juicessh-plugin-library")' build.gradle
    buildjni=no

Build:1.1-Beta-2,8
    commit=v1.1-Beta-2-release
    subdir=app
    gradle=yes
    srclibs=juicessh-plugin-library@cf1a9c3494f15c324fbb1e373b49b1b3a79b1926
    prebuild=sed -i -e '/raw.github.com\/Sonelli\/maven/d' -e '/juicessh/d' build.gradle && \
        sed -i -e '/jcenter/amavenLocal()' ../build.gradle && \
        mkdir -p ../libs/ && \
        pushd $$juicessh-plugin-library$$ && \
        sed -i -e '/Dropbox/d' -e '/groupId/irepository(url: mavenLocal().url)' build.gradle && \
        popd && \
        cp -fR $$juicessh-plugin-library$$ ../libs/ && \
        echo -e "\ninclude ':libs:juicessh-plugin-library'" >> ../settings.gradle && \
        sed -i -e '/support-v4/acompile project(":libs:juicessh-plugin-library")' build.gradle
    buildjni=no

Auto Update Mode:Version v%v-release
Update Check Mode:Tags
Update Check Name:biz.incomsystems.fwknop2
Current Version:1.1-Beta-2
Current Version Code:8

