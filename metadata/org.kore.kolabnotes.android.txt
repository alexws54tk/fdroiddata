Categories:Office
License:LGPLv3
Web Site:
Source Code:https://github.com/konradrenner/kolabnotes-android
Issue Tracker:https://github.com/konradrenner/kolabnotes-android/issues

Auto Name:Kolab Notes
Summary:Write notes and sync them with Kolab
Description:
Take notes locally and optionally sync them with Kolab and IMAP servers in
the Kolab v3 storage format.

The goal of the app is, to support the full stack of Kolab Notes
functionality: notebooks and tags for organising notes is already possible.
Also it is possible to edit the classification and color of a note. Formating
the text and inserting inline images works, too.

There are already 2 types of widgets included in the app. One widget is a
kind of a "sticky note" widget, it displays the summary and description of
a single note. The other widget is a configureable list widget.
.

Repo Type:git
Repo:https://github.com/konradrenner/kolabnotes-android

Build:0.1.0-alpha4,4
    commit=0.1.0-alpha4
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.3
    prebuild=sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' build.gradle 

Build:0.1.0-beta1,7
    commit=0.1.0-beta1
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.3,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Build:0.1.0-beta2,8
    commit=0.1.0-beta2
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.3,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Build:0.1.0-beta4,10
    commit=0.1.0-beta4
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.3,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Build:0.1.0-beta5,11
    commit=0.1.0-beta5
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.4,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Build:0.1.0-beta6,12
    commit=0.1.0-beta6
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.4,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Build:0.1.0-beta8,14
    commit=0.1.0-beta8
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.4,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Build:0.1.0-beta9,15
    commit=0.1.0-beta9
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.4,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Build:0.1.0-beta10,16
    commit=0.1.0-beta10
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@0.6.5,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Build:0.1.0,17
    commit=0.1.0
    subdir=app
    gradle=yes
    srclibs=KolabNotesLib@1.0.0,ColorPicker-ambilwarna@a8d92e310f7b7419852b5367c1560f5db2b4fb98
    prebuild=mkdir -p ../libs/ && \
        sed -i -e '/jitpack/,+1d' -e '/maven {/d' -e '//amavenLocal()' ../build.gradle && \
        pushd $$KolabNotesLib$$ && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e 's/com.github.konradrenner/org.kore.kolab.notes/g' -e '/ambilwarna/d' -e '/support-v4/acompile project(":libs:colorpicker:library")\n' build.gradle && \
        cp -fR $$ColorPicker-ambilwarna$$ ../libs/colorpicker && \
        echo -e "\ninclude ':libs:colorpicker:library'" >> ../settings.gradle && \
        sed -i -e 's/propBuildToolsVersion/22.0.1/g' -e 's/propCompileSdkVersion/22/g' -e 's/propMinSdkVersion/7/g' -e 's/propTargetSdkVersion/22/g' -e 's/propVersionCode/2/g' -e 's/propVersionName/"2.0"/g' ../libs/colorpicker/library/build.gradle

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1.0
Current Version Code:17

