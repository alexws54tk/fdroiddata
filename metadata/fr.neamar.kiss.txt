Categories:System
License:MIT
Web Site:https://github.com/Neamar/KISS/blob/HEAD/readme.md
Source Code:https://github.com/Neamar/KISS
Issue Tracker:https://github.com/Neamar/KISS/issues

Auto Name:KISS launcher
Summary:Custom launcher
Description:
KISS is a fast launcher following the KISS principle.

Search through you app, contacts and settings lightning fast. No more time
spent trying to find the app you need to launch: enter a few characters
from the name and press enter. Need to phone someone? Don't meddle with
the call log, just give three letters of their name and push the "phone"
button.

KISS becomes smarter and smarter as you use it, pushing forward results
you're more likely to select.
.

Repo Type:git
Repo:https://github.com/Neamar/KISS

Build:2.6.0,44
    commit=v2.6.0
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:2.6.0
Current Version Code:44

