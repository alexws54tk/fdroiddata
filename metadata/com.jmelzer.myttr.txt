AntiFeatures:NonFreeNet
Categories:Office,Internet,Games
License:Apache2
Web Site:
Source Code:https://github.com/chokdee/myTTR
Issue Tracker:https://github.com/chokdee/myTTR/issues

Auto Name:myTTR
Summary:Table tennis rating and community
Description:
Search [http://mytischtennis.de myTT] for players and teams, check
how many points can be scored and have a quick look at the stats.
.

Repo Type:git
Repo:https://github.com/chokdee/myTTR

Build:0.11,8
    commit=7e3b6aaa8374e36c2ecffa16bc6f02263de164a7
    subdir=myTTR
    gradle=yes

Build:1.0,9
    commit=7c0971249d37d32bc10bcd7be52b8e7408330bbe
    subdir=myTTR
    gradle=yes

Build:1.0.1,10
    commit=v1.0.1
    subdir=myTTR
    gradle=yes

Build:1.0.2,11
    commit=v1.0.2
    subdir=myTTR
    gradle=yes

Build:1.5.0,12
    commit=v1.5.0
    subdir=myTTR
    gradle=yes

Build:1.6.0,15
    commit=1.6.0
    subdir=myTTR
    gradle=yes

Build:2.0.0,20
    commit=v2.0.0
    subdir=myTTR
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.0.0
Current Version Code:20

