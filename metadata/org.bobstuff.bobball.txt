Categories:Games
License:FreeBSD
Web Site:https://github.com/bobthekingofegypt/BobBall/blob/HEAD/README.mkd
Source Code:https://github.com/bobthekingofegypt
Issue Tracker:https://github.com/bobthekingofegypt/BobBall/issues

Auto Name:BobBall
Summary:Jezzball style clone
Description:
BobBall is a simple version of the old game Jezzball.

Features:
* Easy to play, hard to master
* Infinite levels
* Top scores table
.

Repo Type:git
Repo:https://github.com/bobthekingofegypt/BobBall.git

Build:1.0,1
    commit=v1.0

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1

